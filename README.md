# UE4 大型项目插件指南

本指南主要用于使用 UE4 开发大型项目时，旨在帮助插件编写更为规范。

(持续更新并修正中..)

## 1. 插件目录结构

- [`PluginName`]/
  - Content/
  - Resource/
  - Source/
    - ThirdParty/<sup>_2.1_</sup>
    - [`ThirdPartyName`]/
      - Inc/<sup>_2.2_</sup>
      - Lib/<sup>_2.3_</sup>
        - x64/
        - x86/
    - [`PluginName`]/<sup>_2.4_</sup>
      - Classes/<sup>_2.5_</sup>
      - Private/<sup>_2.6_</sup>
        - S[`PluginName`]<sup>_2.7_</sup>
      - Public/<sup>_2.8_</sup>
      - [`PluginName`].Build.cs<sup>_2.9_</sup>
    - [`PluginName`]Editor/<sup>_2.10_</sup>
      - Private/<sup>_2.11_</sup>
        - S[`PluginName`]Editor/<sup>_2.12_</sup>
      - Public/<sup>_2.13_</sup>
      - [`PluginName`]Editor.Build.cs<sup>_2.14_</sup>
    - [`PluginName`]Slate/<sup>_2.15_</sup>
      - Private/<sup>_2.16_</sup>
      - Public/<sup>_2.17_</sup>
      - [`PluginName`]Slate.Build.cs
  - [`PluginName`].uplugin<sup>_2.18_</sup>

## 2. 目录内容

> 2.1. 文件夹下的目录结构可能会因为实际情况需要自行调整  
> 2.2. SDK 的 API  
> 2.3. 静态链接库，Windows 下一般需要根据系统选择引用目录

> 2.4. Runtime 插件部分
> 2.5. Runtime 下的类定义  
> 2.6. 所有.cpp 文件，以及部分不公开的.h 文件  
> 2.7. Runtime 的 Slate 部分（runtime 一般比较少用到，使用 UMG 会更多）  
> 2.8. 需要被外部使用的.h 文件，原则上应仅包含接口文件，以及一些函数库  
> 2.9. 第三方库的引用在这里定义

> 2.10. Editor 插件部分  
> 2.11. 同 2.6  
> 2.12. Editor 的 Slate 部分（通常在使用 StandAloneWindow 或者 CustomDetails 的时候会用到）  
> 2.13. 同 2.8  
> 2.14. 一般会依赖 Runtime 和 Slate 模块

> 2.15. Slate 插件部分，通用 Slate，编写时不引用 Runtime 和 Editor 部分  
> 2.16. 同 2.6  
> 2.17. 同 2.8

> 2.18. 插件描述，如果依赖于其他插件，需要列出，有需要的情况下还需指出 whitelist

## 3. 其他

> 3.1. 项目文件按需添加到上述文件目录中  
> 3.2. 项目文件指南：
>
> > 3.2.1. 常量和 enum:  
> > 建议新建 **[`PluginName`]Defines.h** 的文件，相关定义放在这里  
> > 如果需要定义特定的常量或enum, 可以考虑新建以 **Defines.h** 结尾的文件  
> > 3.2.2. struct:  
> > 建议新建 **[`PluginName`]Types.h** 的文件，相关数据结构放在这里  
> > 如果需要定义特定的struct, 可以考虑新建以 **Types.h** 结尾的文件
> > 结构体可以引用类成员，但是建议不要涉及类相关的操作部分，如果需要使用类操作，建议放在类文件里  
> > 3.2.3. macros:  
> > 建议新建 **[`PluginName`]Macros.h** 的文件，把通用宏写在里面，建议模块唯一  
> > 如果是一些 UE 相关的全局消息 DELEGATE，建议新建 **[`PluginName`]Delegates.h** 的文件，相关消息代理放在里面，建议模块唯一  
> > 3.2.4. BlueprintLibrary:  
> > 建议新建 **[`PluginName`]BPLib.h** 的文件，把需要暴露给蓝图的函数库放在里面，建议模块唯一  
> > 3.2.5. Helpers/Utils:  
> > 不唯一，不需要暴露给蓝图的辅助函数库，按需新建 **[`About`]Helpers.h** 的文件
>
> 3.3. 日志:  
> (待定)
